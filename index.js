// const forbidden = require('./resources/forbidden');
const forbidden = [
  "parseInt",
  "document\\.write",
  "eval",
  "isNaN",
  "unescape",
  "escape",
  "parseFloat",
  "parseInt",
  "eval",
  "isNaN",
  "onload",
  "alert",
  "script",
  "<",
  ">",
  "onload",
  "=",
  "\\(",
  "\\)",
  '\\"',
  "/"
];

// forbidden guarda palabras en un arreglo y evita que se utilicen en la pagina



const express = require ('express');
const app=express();
const es6Renderer= require('express-es6-template-engine')
const cors = require('cors')
const bodyParser = require('body-parser');
reemplazarTodos = (palabra,reemplazar,reemplazo) =>{
  while(palabra.includes(reemplazar)){
      console.log(`Se ha reemplazado ${reemplazar} en ${palabra}`)
      palabra = palabra.replace(reemplazar,reemplazo)
  }
  return palabra
}

limpiarRequest = (data) => {
  const newData = {};
  Object.keys(data).forEach((key) => {
    let value = data[key];
    if (typeof value === "string") {
      forbidden.forEach((word) => {
        value = value.replace(new RegExp(word, "gi"), "");
      });
    }
    newData[key] = value;
  });
  return newData;
};
app.use(express.json());
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.set('views','views');
app.set('view engine','html')
app.engine('html',es6Renderer)
const {Configuration , OpenAIApi} = require("openai");
const { request } = require('express');
const { response } = require('express');
const configuration = new Configuration({
    apiKey: "ingresar api key"
})
const openai = new OpenAIApi(configuration)
app.get('/index',(request , response)=>{
    response.render('index' );
  
})
app.get('/index',(request , response)=>{
  response.render('index' );

})



app.get('/', (req, res) => {
  res.send('Bienvenido al sistema de jubilaciones de Chile');
});

//no api

function manejarSituacion(app) {
  return async function (request, response) {
    const toSend = limpiarRequest({
      Sexo: request.body.Sexo,
      Edad: request.body.Edad,
    });

    if (toSend.Edad >= 18) {
      toSend.Mensaje = `Ud es ${toSend.Sexo} y tiene ${toSend.Edad} años. 
      `
      toSend.Mensaje2 =" Usted puede obtener una licencia de conducir."

      if (
        (toSend.Sexo === "hombre" && toSend.Edad >= 18 && toSend.Edad <= 64) ||
        (toSend.Sexo === "mujer" && toSend.Edad >= 18 && toSend.Edad <= 59)
      ) {
        toSend.Mensaje3 = "aun no tiene la edad suficiente de jubilacion";
      } else {
        toSend.Mensaje3 = "usted esta en edad de jubilacion";
      }
    } else {
      toSend.Mensaje =
        "Lo siento, usted es menor de edad y no puede obtener una licencia de conducir."; //esto no se pedia pero igual lo coloque
    }

    console.log(toSend);


    // aqui puse dos response uno .json y el otro normal
    // vista  esto como html este no funciona en el thunderclient no muestra el response
    response.render("situacion", {
      locals: toSend,
    });

    //json esto pasa los datos con formato json y funciona en el thunderclient
    // response.json(toSend)
  };
}

  app.post('/situacion', async (request, response) => {
    console.log('Peticion recibida en /situacion manual')
    manejarSituacion(app)(request, response);
    
  });
  // al final use esto para hacer la funcion de segurizar con la api ya que de la otra forma que lo intente no funciono
  app.post('/buscar',async (request,response) =>{
    pregunta=` contiene lenguaje 
    javascript en la siguiente expresion? "${request.body.buscar}". responde true o false en minusculas`
    console.log(pregunta);
    const completion = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: pregunta
    })
    console.log(completion.data.choices[0].text);
    if(completion.data.choices[0].text.trim()==  "true"){
        response.render('buscar',{locals: {busqueda:"busqueda invalida"}
  
        })
    }else{
        response.render('buscar',{locals: {busqueda: request.body.buscar}
  
        })
    }  
  })
  
  
  
app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
  });
