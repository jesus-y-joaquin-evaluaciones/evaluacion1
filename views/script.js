const enviarFormulario = (ruta) => {
  const formulario = document.getElementById('formulario');
  const datosFormulario = new FormData(formulario);
  
  fetch(ruta, {
    method: 'POST',
    body: datosFormulario
  })
  .then(response => response.json())
  .then(data => {
    // Manejar la respuesta del servidor aquí
  })
  .catch(error => {
    console.error('Error al enviar solicitud POST', error);
  });
}

const botonManual = document.getElementById('Manual');
botonManual.addEventListener('click', (event) => {
  event.preventDefault(); // Evita que se envíe el formulario por defecto
  enviarFormulario('/situacion');
});

